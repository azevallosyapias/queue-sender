package com.queue.sender;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SenderApplication {

	static final Logger LOG = LoggerFactory.getLogger(SenderApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(SenderApplication.class, args);
	}

}
