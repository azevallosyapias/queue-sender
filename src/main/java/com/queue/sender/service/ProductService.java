package com.queue.sender.service;

import com.queue.sender.model.Product;
import com.queue.sender.model.Response;

public interface ProductService {

	public Response sendProduct(Product product);

}
