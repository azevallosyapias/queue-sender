package com.queue.sender.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.queue.sender.message.MessageSender;
import com.queue.sender.model.Product;
import com.queue.sender.model.Response;
import com.queue.sender.util.Constants;

@Service("productService")
public class ProductServiceImpl implements ProductService {

	static final Logger LOG = LoggerFactory.getLogger(ProductServiceImpl.class);

	@Autowired
	MessageSender messageSender;

	@Override
	public Response sendProduct(Product product) {
		Response res = new Response();
		LOG.info("--------");
		LOG.info("Application : sending order request {}", product);
		try {
			messageSender.sendMessage(product);
			res.setCode(Constants.OK);
			res.setMessage("Sent successfully");
		} catch (Exception e) {
			LOG.error("Failed to send object to Queue", e);
			res.setCode(Constants.ERROR);
			res.setMessage("Failed to send object to Queue");
		}		
		LOG.info("--------");
		
		return res;
	}

}
