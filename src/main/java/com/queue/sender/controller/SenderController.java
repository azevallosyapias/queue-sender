package com.queue.sender.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.queue.sender.model.Product;
import com.queue.sender.model.Response;
import com.queue.sender.service.ProductServiceImpl;

@RestController
@RequestMapping("/api/v1/")
public class SenderController {

	static final Logger LOG = LoggerFactory.getLogger(SenderController.class);

	@Autowired
	ProductServiceImpl productServiceImpl;

	@PostMapping(path = "/pushProductQueue", consumes = "application/json", produces = "application/json")
	public Response envioCorreo(@RequestBody Product product) {
		
		LOG.debug("filtros={}", new Gson().toJson(product));
		return productServiceImpl.sendProduct(product);

	}
}
