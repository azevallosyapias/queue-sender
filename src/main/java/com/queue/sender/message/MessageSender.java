package com.queue.sender.message;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Component;

import com.queue.sender.model.Product;

@Component
public class MessageSender {

//	@Autowired
//	JmsTemplate jmsTemplate;
	@Autowired
	@Qualifier("connectG")
	ConnectionFactory cf;

	public void sendMessage(final Product product) {

		// manage template for send easily
		JmsTemplate template = new JmsTemplate();
		template.setConnectionFactory(cf);
		template.setDefaultDestinationName("order-queue_1");

		template.send(new MessageCreator() {
			@Override
			public Message createMessage(Session session) throws JMSException {
				ObjectMessage objectMessage = session.createObjectMessage(product);
				return objectMessage;
			}
		});
	}

}
