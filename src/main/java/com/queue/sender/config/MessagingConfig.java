package com.queue.sender.config;

import java.util.Arrays;

import javax.jms.ConnectionFactory;

import org.apache.activemq.spring.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.connection.CachingConnectionFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.listener.DefaultMessageListenerContainer;
import org.springframework.jms.listener.MessageListenerContainer;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.SimpleMessageConverter;

import com.queue.sender.message.MessageReceiver;

@Configuration
public class MessagingConfig {

	@Value("${queue.host}")
	private String hostQueue;

	@Value("${queue.port}")
	private String portQueue;

	// private static final String DEFAULT_BROKER_URL = "tcp://localhost:61616";

	private static final String ORDER_QUEUE = "order-queue_1";
	private static final String ORDER_RESPONSE_QUEUE = "order-response-queue";

	static final Logger LOG = LoggerFactory.getLogger(MessagingConfig.class);

	@Autowired
	MessageReceiver messageReceiver;

	@Bean
	@Qualifier("connectG")
	public ConnectionFactory connectionFactory() {
		ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory();
		LOG.info("----> RUTA COLA: " + "tcp://" + hostQueue + ":" + portQueue);
		connectionFactory.setBrokerURL("tcp://" + hostQueue + ":" + portQueue);
		connectionFactory.setTrustedPackages(Arrays.asList("com.queue.sender"));
		return connectionFactory;
	}
	/*
	 * Optionally you can use cached connection factory if performance is a big
	 * concern.
	 */

	@Bean
	@Qualifier("connectCache")
	public ConnectionFactory cachingConnectionFactory() {
		CachingConnectionFactory connectionFactory = new CachingConnectionFactory();
		connectionFactory.setTargetConnectionFactory(connectionFactory());
		connectionFactory.setSessionCacheSize(10);
		return connectionFactory;
	}

	/*
	 * Message listener container, used for invoking messageReceiver.onMessage
	 * on message reception.
	 */
	@Bean
	public MessageListenerContainer getContainer() {
		DefaultMessageListenerContainer container = new DefaultMessageListenerContainer();
		container.setConnectionFactory(connectionFactory());
		container.setDestinationName(ORDER_RESPONSE_QUEUE);
		container.setMessageListener(messageReceiver);
		return container;
	}

	/*
	 * Used for Sending Messages.
	 */
	@Bean
	public JmsTemplate jmsTemplate() {
		JmsTemplate template = new JmsTemplate();
		template.setConnectionFactory(connectionFactory());
		template.setDefaultDestinationName(ORDER_QUEUE);
		return template;
	}

	@Bean
	MessageConverter converter() {
		return new SimpleMessageConverter();
	}

}
