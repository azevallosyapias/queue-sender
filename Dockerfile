# para copiar las fuentes y generar el .jar
FROM maven:3.6-jdk-8-alpine AS builder
WORKDIR /app

COPY pom.xml .
RUN mvn -B -e -C -T 1C org.apache.maven.plugins:maven-dependency-plugin:3.0.2:go-offline

COPY src ./src
RUN mvn clean package -Dmaven.test.skip=true


# generando el JAR
FROM openjdk:8-jdk-alpine
LABEL maintainer="alonsofisi@gmail.com"
WORKDIR /workspace

ENV portQ="61616"
ENV hostQ="hostQ1"

COPY --from=builder /app/target/sender*.jar sender.jar

ENTRYPOINT exec java -jar /workspace/sender.jar

EXPOSE 2020
# NO PONER '_' EN LOS LINKS DE VARIABLES ya que SALE EL ERROR java.lang.IllegalArgumentException
# para correr el script
#sin network
# docker run -d --name hostQ -p 61616:61616 -p 8161:8161 rmohr/activemq
# docker run --name send2 --link hostQ:hostQ1 -p2021:2020  alonsozy/sender1:1.0.2



#crear network colas
# docker network create network
# docker run -d --name hostQ --network=colas -p 61616:61616 -p 8161:8161 rmohr/activemq
# docker run --name send2 --network=colas -p2020:2020  alonsozy/sender1:1.0.1


#FROM openjdk:8-jdk-alpine
#LABEL maintainer="alonsofisi@gmail.com"
#WORKDIR /workspace
#COPY target/sende*.jar app1.jar
#ENTRYPOINT exec java -Djava.security.egd=file:/dev/./urandom -jar /workspace/app1.jar
#EXPOSE 2020